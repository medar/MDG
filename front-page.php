<?php get_header(); ?>
<main class="site-main site-main--index">
	<div class="index-banner">
		<div class="index-banner__img">
			<picture>
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/1.jpg 1x, <?php echo get_template_directory_uri(); ?>/img/index/1x2.jpg 2x"
						media="(min-width: 1200px)">
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/1--768.jpg 1x, <?php echo get_template_directory_uri(); ?>/img/index/1--768x2.jpg 2x"
						media="(min-width: 768px)">
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/1--480.jpg 1x, <?php echo get_template_directory_uri(); ?>/img/index/1--480x2.jpg 2x"
						media="(min-width: 480px)">
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/1--479.jpg 1x, <?php echo get_template_directory_uri(); ?>/img/index/1--479x2.jpg 2x"
						media="(max-width: 479px)">
				<img src="<?php echo get_template_directory_uri(); ?>/img/index/1.jpg" alt="">
			</picture>
			<div class="index-banner__text">
				<h1><?php echo get_bloginfo( 'name' ); ?></h1>
				<?php
				if ( function_exists( 'ot_get_option' ) ) :
					echo '<a class="btn btn--white" href="' . ot_get_option( 'giftcard_btn_link' ) . '">' . ot_get_option( 'giftcard_btn_title' ) .'</a>';
				endif;
				?>
			</div>
		</div>
	</div>
	<?php get_template_part( 'template-parts/site-links' ); ?>
	<div class="index-about">
		<div class="index-about__text">
			<div class="container">
				<?php
				while ( have_posts() ) :
					the_post(); ?>
					<?php the_content();
				endwhile;
				?>
			</div>
		</div>
		<?php get_template_part( 'template-parts/about-photo' ); ?>
	</div>
	<div class="index-giftcards">
		<div class="container">
			<?php
			if ( function_exists( 'ot_get_option' ) ) :
				echo '<h2>' . ot_get_option( 'header_after_backie_and_debbie_photos' ) . '</h2>';
				echo '<p>' . ot_get_option( 'text_after_backie_and_debbie_photos' ) . '</p>';
				echo '<a class="btn btn--white" href="' . ot_get_option( 'giftcard_btn_link' ) . '">' . ot_get_option( 'giftcard_btn_title' ) .'</a>';
			endif;
			?>
		</div>
	</div>
	<div class="index-contact">
		<div class="container">
			<h2>Contact Us</h2>
			<?php echo do_shortcode( '[contact-form-7 id="43" title="Front contact form"]' ) ?>
		</div>
	</div>
	<?php get_template_part( 'template-parts/recent-posts' ); ?>
	<?php get_template_part( 'template-parts/subscribe' ); ?>
</main>
<?php get_footer(); ?>
