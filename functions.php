<?php
/**
 * MDG functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

if ( ! function_exists( 'mdg_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function mdg_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/*
		 * This theme uses wp_nav_menu() in one location.
		 */
		register_nav_menus( array(
			'menu-1' => esc_html( 'Primary' ),
			'menu-2' => esc_html( 'Footer menu' )
		) );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'post-header-full', 1170, 500 );
		add_image_size( 'post-preview', 340, 300, true );
	}
endif;
add_action( 'after_setup_theme', 'mdg_setup' );

if ( ! function_exists( 'mdg_scripts' ) ) :
	function mdg_scripts() {

		wp_enqueue_style(
			'mdg-libs',
			get_template_directory_uri() . '/css/libs.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'mdg-styles',
			get_template_directory_uri() . '/css/styles.css',
			array( 'mdg-libs' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'mdg-fonts',
			'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700|Roboto+Condensed:700',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'mdg-style',
			get_stylesheet_uri(),
			array( 'mdg-libs', 'mdg-styles' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_script(
			'mdg-libs-js',
			get_template_directory_uri() . '/js/libs.js',
			array( 'jquery' ),
			'1.0.0',
			true
		);

		wp_enqueue_script(
			'mdg-js',
			get_template_directory_uri() . '/js/scripts.js',
			array( 'mdg-libs-js' ),
			'1.0.0',
			true
		);
	}
endif;

add_action( 'wp_enqueue_scripts', 'mdg_scripts' );

function mdg_recent_posts( $posts_count = 3, $posts_offset = 0, $category_name = 'blog', $order_by = 'date' ) {
	$order = '';
	if ( $order_by == 'date' ) {
		$order = "DESC";
	} elseif ( $order_by == 'title' ) {
		$order = "ASC";
	}
	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

	$args  = array(
		'posts_per_page'   => $posts_count,
		'post_type'        => 'post',
		'category_name'    => $category_name,
		'post_status'      => 'publish',
		'paged'            => $paged,
		'orderby'          => $order_by,
		'order'            => $order,
		'offset'           => $posts_offset,
		'suppress_filters' => true

	);
	$query = new WP_Query( $args );
	global $post;

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$post = $query->post;
			setup_postdata( $post );

			if ( has_post_thumbnail( $post->ID ) ) :
				get_template_part( 'template-parts/post-preview' );
			else :
				get_template_part( 'template-parts/post-preview-no-img' );
			endif;
		}
		wp_reset_query();
		wp_reset_postdata();
	} else {
		wp_reset_query();
		wp_reset_postdata();
	}
}

function mdg_search_posts() {
	global $post;

	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			setup_postdata( $post );

			if ( has_post_thumbnail( $post->ID ) ) :
				get_template_part( 'template-parts/post-preview' );
			else :
				get_template_part( 'template-parts/post-preview-no-img' );
			endif;
		}
		wp_reset_postdata();
	} else {
		get_template_part( 'template-parts/no-posts' );
		wp_reset_postdata();
	}
	wp_reset_query();
}

function mdg_posted_on( $post_date ) {
	$time_string = date( 'd M Y', strtotime( $post_date ) );

	echo '<span class="article-date">' . $time_string . '</span>';

}

function mdg_sidebar( $name = '' ) {
	do_action( 'get_sidebar', $name );

	if ( $name ) {
		$name = "-$name";
	}

	locate_template( "template-parts/sidebar$name.php", true );
}

/**
 * ajax коллбэк для infinite scroll
 */
function ajax_sw_infinite_scroll_callback() {
	$offset  = intval( $_POST['offset'] );
	$sort_by = strval( $_POST['sort_by'] );
	mdg_recent_posts( $posts_count = 3, $posts_offset = $offset, $category_name = 'blog', $order_by = $sort_by );

	wp_die();
}

// Change search function globally to search only post, page and portfolio post types
function prefix_limit_post_types_in_search( $query ) {
	if ( $query->is_search ) {
		$query->set( 'category_name', 'blog' );
	}

	return $query;
}

add_filter( 'pre_get_posts', 'prefix_limit_post_types_in_search' );

function mdg_register_types() {
}

add_action( 'init', 'mdg_register_types' );

add_action( 'wp_ajax_infinite_scroll', 'ajax_sw_infinite_scroll_callback' );
add_action( 'wp_ajax_nopriv_infinite_scroll', 'ajax_sw_infinite_scroll_callback' );

function disable_wp_emojicons() {
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}

add_action( 'init', 'disable_wp_emojicons' );
function disable_emojicons_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

add_filter( 'emoji_svg_url', '__return_false' );

function mdg_article_contact_form_shortcode() {
	return do_shortcode( '[contact-form-7 id="109577" title="Article contact form"]' );
}

function mdg_shortcodes_init() {
	add_shortcode( 'contact-form', 'mdg_article_contact_form_shortcode' );
}

add_action( 'init', 'mdg_shortcodes_init', 20 );