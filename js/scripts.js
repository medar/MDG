jQuery(document).ready(function ($) {
// events on load
    window.onload = function () {
        openMenu();
        allSites();
        youtubeInit();
    };


// events on scroll
    $(window).scroll(function () {
        fixedHeader();
    });


// events on resize
    window.onresize = function () {
        fixedHeader();
        closeMenu();
        youtubeResize();
    };


// function declarations
    function openMenu() {
        $('.menu-button').click(function () {
            var menu = $('.menu');
            var btn = $('.menu-button');
            var main = $('.site-main');
            if (btn.hasClass('is-closed')) {
                btn.removeClass('is-closed');
                menu.addClass('is-open');
                main.addClass('menu-is-open');
            }
            else {
                btn.addClass('is-closed');
                menu.removeClass('is-open');
                main.removeClass('menu-is-open');
            }
        });
    }

    function closeMenu() {
        if ($(window).width() > '1199') {
            var menu = $('.menu');
            var btn = $('.menu-button');
            var main = $('.site-main');
            btn.addClass('is-closed');
            menu.removeClass('is-open');
            main.removeClass('menu-is-open');
        }
    }

    function allSites() {
        $('body').waypoint({
            handler: function (direction) {
                if (direction === 'down') {
                    $('#allSites').addClass('active');
                }
                if (direction === 'up') {
                    $('#allSites').removeClass('active');
                }
            },
            offset: '-100%'
        });
    }

    function fixedHeader() {
        var scrollPosition = $(window).scrollTop();
        var fixedHeaderMenu = $('.site-header');
        var fixedStartPosition = 1;
        if ((scrollPosition >= fixedStartPosition) && ($(window).width() > '1200')) {
            fixedHeaderMenu.addClass("is-fixed");
        } else {
            fixedHeaderMenu.removeClass("is-fixed");
        }
    }

    function youtubeInit() {
        $(".youtube").each(function () {
            $(this).width($(this).parent().width()).height($(this).parent().width() * .563);
            $(this).css('background-image', 'url(https://i.ytimg.com/vi/' + this.id + '/hqdefault.jpg)');
            $(this).append($('<div/>', {'class': 'play'}));
            $(document).delegate('#' + this.id, 'click', function () {
                var iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1";
                if ($(this).data('params')) iframe_url += '&' + $(this).data('params');
                var iframe = $('<iframe/>', {
                    'allowfullscreen': '1',
                    'frameborder': '0',
                    'src': iframe_url,
                    'width': $(this).parent().width(),
                    'height': $(this).parent().width() * .563
                });
                $(this).replaceWith(iframe);
            });
        });
    }

    function youtubeResize() {
        $(".youtube").each(function () {
            var $el = $(this);
            $el
                .width($(this).parent().width())
                .height($(this).parent().width() * .563);
        });
    }

    var $infinitescroll_btn = $('#infinitescroll-on_click');

    hide_infinite({action: 'infinite_scroll', offset: get_offset(), sort_by: $infinitescroll_btn.attr("data-sort")});

    function get_offset() {
        return $('.latest-blog-item').length;
    }

    $infinitescroll_btn.on('click', function (event) {
        event.preventDefault();
        var order_by = $(this).attr("data-sort");
        var data = {
            action: 'infinite_scroll',
            offset: get_offset(),
            sort_by: order_by
        };
        jQuery.post(ajaxurl, data, function (response) {
            $('.latest-blog-posts__wrapper').append(response);
            Waypoint.refreshAll();
            hide_infinite({action: 'infinite_scroll', offset: get_offset(), sort_by: order_by});
        });
    });

    function hide_infinite(data) {
        jQuery.post(ajaxurl, data, function (response) {
            if (response.length < 1) {
                $infinitescroll_btn.hide();
            }
        });
    }

});