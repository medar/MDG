<?php
/**
 * Template Name: About Us
 *
 * Description: Template for About Us page
 */
get_header(); ?>

	<main class="site-main site-main--index">
		<div class="about-content">
			<div class="container">
				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			</div>
			<?php get_template_part( 'template-parts/about-photo' ); ?>
		</div>
		<?php get_template_part( 'template-parts/recent-posts' ); ?>
		<?php get_template_part( 'template-parts/subscribe' ); ?>
	</main>


<?php
get_footer();
