<?php
/**
 * Template Name: Coming Soon
 *
 * Description: Template for Coming Soon page
 */
get_header(); ?>

	<main class="site-main site-main--index">
		<?php get_template_part( 'template-parts/coming-soon' ); ?>
		<?php get_template_part( 'template-parts/recent-posts' ); ?>
		<?php get_template_part( 'template-parts/subscribe' ); ?>
	</main>


<?php
get_footer();
