<?php
get_header(); ?>

	<main class="site-main site-main--index">
		<div class="container">
			<?php get_template_part( 'template-parts/coming-soon', 'page' ); ?>
		</div>
		<?php get_template_part( 'template-parts/recent-posts' ); ?>
		<?php get_template_part( 'template-parts/subscribe' ); ?>
	</main>


<?php
get_footer();
