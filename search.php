<?php get_header(); ?>
	<main class="site-main site-main--blog clearfix">
		<?php get_template_part( 'template-parts/site-links' ); ?>
		<div class="blog-title">
			<div class="container">
				<div class="blog-title__wrapper search-title__wrapper">
					<h1>Search Results</h1>
				</div>
			</div>
		</div>

		<div class="blog-items latest-blog-posts">
			<div class="container">
				<div class="latest-blog-posts__wrapper">
					<?php mdg_search_posts(); ?>
				</div>
			</div>
		</div>
	</main>
<?php get_footer(); ?>