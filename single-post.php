<?php
get_header();
?>

	<main class="site-main site-main--article clearfix">
		<?php

		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'article' );

		endwhile;
		?>

		<?php get_template_part( 'template-parts/recent-posts' ); ?>
		<?php get_template_part( 'template-parts/subscribe' ); ?>
	</main>

<?php
get_footer();