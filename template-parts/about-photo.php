<?php
/**
 * Template part for displaying photos
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
?>
<div class="index-about__photo">
	<div class="container">
		<div class="index-about__photo-wrapper">
			<picture>
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/Backie.png 1x, <?php echo get_template_directory_uri(); ?>/img/index/Backie@x2.png 2x"
				        media="(min-width: 1200px)">
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/Backie--768.png 1x, <?php echo get_template_directory_uri(); ?>/img/index/Backie@x2--768.png 2x"
				        media="(min-width: 480px)">
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/Backie--480.png 1x, <?php echo get_template_directory_uri(); ?>/img/index/Backie@x2--480.png 2x"
				        media="(max-width: 479px)">
				<img src="<?php echo get_template_directory_uri(); ?>/img/index/Backie.png" alt="">
			</picture>
			<picture>
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/Debbie.png 1x, <?php echo get_template_directory_uri(); ?>/img/index/Debbie@x2.png 2x"
				        media="(min-width: 1200px)">
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/Debbie--768.png 1x, <?php echo get_template_directory_uri(); ?>/img/index/Debbie@x2--768.png 2x"
				        media="(min-width: 480px)">
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/Debbie--480.png 1x, <?php echo get_template_directory_uri(); ?>/img/index/Debbie@x2--480.png 2x"
				        media="(max-width: 479px)">
				<img src="<?php echo get_template_directory_uri(); ?>/img/index/Debbie.png" alt="">
			</picture>
		</div>
	</div>
</div>
