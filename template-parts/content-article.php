<?php
/**
 * Template part for displaying content in article
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
if ( ! has_post_thumbnail() ) {
	$no_image = ' article-img--without-img';
} else {
	$no_image = '';
}
?>
<div class="article-img">
	<div class="container">
		<div class="row">
			<div class="article-img__wrapper <?php echo $no_image; ?>">
				<?php
				if ( has_post_thumbnail() ) {
					echo '<picture>';
					echo get_the_post_thumbnail( $post->ID, array( 1170, 500 ) );
					echo '</picture>';
				} ?>
				<div class="article-img__title">
					<h1><?php the_title(); ?></h1>
					<p><?php mdg_posted_on( $post->post_date ); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="article-content">
	<div class="article-content__main">
		<?php the_content(); ?>
	</div>
	<?php mdg_sidebar('article'); ?>
</div>