<?php
/**
 * Template part for displaying content in page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
while ( have_posts() ) :
	the_post(); ?>
	<h1><?php the_title(); ?></h1>
	<?php the_content();
endwhile;
?>