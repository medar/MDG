<?php
$search_result = '';
if ( isset( $_GET['s'] ) ):
	$search_result = esc_html($_GET['s']);
endif
?>
<div class="no-posts-block">
<p>Your search - <b><?php echo $search_result; ?></b> - did not match any documents.</p>
<p>Suggestions:</p>
<ul>
	<li>Make sure  all words are spelled correctly.</li>
	<li>Try different keywords.</li>
	<li>Try more general keywords.</li>
</ul>
</div>
