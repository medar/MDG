<?php
/**
 * Template part for displaying preview of post
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
global $post;
$content = strip_shortcodes($post->post_content);
$excerpt = wp_trim_words( $content, $num_words = 15, $more = null );
$time = date( 'd M Y', strtotime( $post->post_date ) );
?>
<div class="latest-blog-item">
	<a class="latest-blog-item__img" href="<?php echo get_permalink($post->ID); ?>">
		<?php echo get_the_post_thumbnail( $post->ID, $size = array(340,300) ); ?>
	</a>
	<span><?php echo $time; ?></span>
	<a href="<?php echo get_permalink( $post->ID ); ?>">
		<h3><?php the_title(); ?></h3>
	</a>
	<p><?php echo $excerpt; ?></p>
</div>