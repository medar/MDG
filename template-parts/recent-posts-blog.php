<?php
/**
 * Template part for displaying preview of posts in blog
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
$sort_by = "date";
if ( isset( $_GET['order_by'] ) and  strval( $_GET['order_by'] ) == 'title' ):
	$sort_by = strval( $_GET['order_by'] );
endif
?>

<div class="blog-items latest-blog-posts">
	<div class="container">
		<div class="latest-blog-posts__wrapper">
			<?php mdg_recent_posts( $posts_count = 9, $posts_offset = 0, $category_name = 'blog', $order_by = $sort_by ); ?>
		</div>
		<a id="infinitescroll-on_click" class="btn btn--turquoise" href="#" data-sort="<?php echo $sort_by; ?>">Show
			More </a>
	</div>
</div>