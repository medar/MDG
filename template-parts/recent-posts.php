<?php
/**
 * Template part for displaying preview of recent posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
?>
<div class="latest-blog-posts">
	<div class="container">
		<div class="latest-blog-posts__wrapper">
			<?php mdg_recent_posts(); ?>
		</div>
	</div>
</div>