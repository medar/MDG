<?php
/**
 * Template part for displaying sidebar in article
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
if ( function_exists( 'ot_get_option' ) ) {
	$facebook_url = ot_get_option( 'social_facebook_link', '#' );
	$twitter_url  = ot_get_option( 'social_twitter_link', '#' );
	$g_plus_url   = ot_get_option( 'social_google_link', '#' );
	$linkedin_url = ot_get_option( 'social_linkedin_link', '#' );
	$photo = ot_get_option( 'main_photo_for_sidebar' );
	$photo_title = ot_get_option( 'title_for_photo_sidebar' );
	$subscribe_form = ot_get_option( 'text_for_subscribe_form' );
	$map_text = ot_get_option( 'text_for_map' );
}
?>
<div class="article-content__sidebar">
	<div class="sidebar-subscribe">
		<?php echo do_shortcode( '[contact-form-7 id="2507" title="Sing up sidebar"]' ); ?>
	</div>
	<div class="sidebar-share">
		<p>Share</p>
		<div class="sidebar-share__wrapper">

			<a href="<?php echo $facebook_url; ?>" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/img/fb.svg" alt="" width="20"
				     height="20">
			</a>
			<a href="<?php echo $twitter_url; ?>" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/img/tw.svg" alt="" width="20"
				     height="18">
			</a>
			<a href="<?php echo $g_plus_url; ?>" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/img/gp.svg" alt="" width="28"
				     height="18">
			</a>
			<a href="<?php echo $linkedin_url; ?>" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/img/li.svg" alt="" width="20"
				     height="18">
			</a>
		</div>

		<?php
		if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar-1' ) ):
		endif;
		?>
	</div>
</div>