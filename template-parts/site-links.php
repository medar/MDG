<?php
/**
 * Template part for displaying site-links
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
if ( function_exists( 'ot_get_option' ) ) {
	$links['mdg']['title']      = ot_get_option( 'mdg_title', '#' );
	$links['mdg']['link']       = ot_get_option( 'mdg_link', '#' );
	$links['dd']['title']       = ot_get_option( 'dd_title', '#' );
	$links['dd']['link']        = ot_get_option( 'dd_link', '#' );
	$links['bpct']['title']     = ot_get_option( 'business_performance_coaching_title', '#' );
	$links['bpct']['link']      = ot_get_option( 'business_performance_coaching_links', '#' );
	$links['swds']['title']     = ot_get_option( 'soho_wellness___day_spa_title', '#' );
	$links['swds']['link']      = ot_get_option( 'soho_wellness___day_spa_link', '#' );
	$links['wandc']['title']    = ot_get_option( 'workshops_and_classes_title', '#' );
	$links['wandc']['link']     = ot_get_option( 'workshops_and_classes_link', '#' );
	$links['pmatcher']['title'] = ot_get_option( 'professional_matcher_title', '#' );
	$links['pmatcher']['link']  = ot_get_option( 'professional_matcher_link', '#' );
}
?>

<div class="index-tabs <?php if (is_home()) : echo 'index-tabs--blog'; endif; ?>">
	<ul>
		<li><a href="<?php echo $links['mdg']['link']; ?>"  target="_blank">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/MDG@2x.png 2x"
					        media="(min-width: 1200px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small@2x.png 2x"
					        media="(max-width: 1199px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG.png"
					     alt="<?php echo $links['mdg']['title']; ?>"
					     title="<?php echo $links['mdg']['title']; ?>">
				</picture>
			</a>
		</li>
		<li><a href="<?php echo $links['dd']['link']; ?>" target="_blank">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/DD.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/DD@2x.png 2x"
					        media="(min-width: 1200px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/DD_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/DD_small@2x.png 2x"
					        media="(max-width: 1199px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/DD.png"
					     alt="<?php echo $links['dd']['title']; ?>"
					     title="<?php echo $links['dd']['title']; ?>">
				</picture>
			</a>
		</li>
		<li><a href="<?php echo $links['bpct']['link']; ?>" target="_blank">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/BC.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/BC@2x.png 2x"
					        media="(min-width: 1200px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/BC_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/BC_small@2x.png 2x"
					        media="(max-width: 1199px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/BC.png"
					     alt="<?php echo $links['bpct']['title']; ?>"
					     title="<?php echo $links['bpct']['title']; ?>">
				</picture>
			</a>
		</li>
		<li><a href="<?php echo $links['swds']['link']; ?>" target="_blank">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/SW.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/SW@2x.png 2x"
					        media="(min-width: 1200px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/SW_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/SW_small@2x.png 2x"
					        media="(max-width: 1199px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/SW.png"
					     alt="<?php echo $links['swds']['title']; ?>"
					     title="S<?php echo $links['swds']['title']; ?>">
				</picture>
			</a>
		</li>
		<li><a href="<?php echo $links['wandc']['link']; ?>" target="_blank">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/WS.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/WS@2x.png 2x"
					        media="(min-width: 1200px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/WS_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/WS_small@2x.png 2x"
					        media="(max-width: 1199px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/WS.png"
					     alt="<?php echo $links['wandc']['title']; ?>"
					     title="<?php echo $links['wandc']['title']; ?>">
				</picture>
			</a>
		</li>
		<li><a href="<?php echo $links['pmatcher']['link']; ?>" target="_blank">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/PM.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/PM@2x.png 2x"
					        media="(min-width: 1200px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/PM_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/PM_small@2x.png 2x"
					        media="(max-width: 1199px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/PM.png"
					     alt="<?php echo $links['pmatcher']['title']; ?>"
					     title="<?php echo $links['pmatcher']['title']; ?>">
				</picture>
			</a>
		</li>
	</ul>
</div>