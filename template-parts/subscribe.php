<?php
/**
* Template part for displaying subscribe part
*
* @link https://codex.wordpress.org/Template_Hierarchy
*/
?>
<div class="subscribe">
	<div class="container">
		<div class="subscribe__wrapper">
			<h2>Sign Up for Newsletter</h2>
			<p>
				<?php
				if ( function_exists( 'ot_get_option' ) ) {
					echo ot_get_option( 'sign_up_for_newsletter', ' ' );
				}
				?></p>
			<?php echo do_shortcode( '[contact-form-7 id="109559" title="Sing up footer"]' ); ?>
		</div>
	</div>
</div>